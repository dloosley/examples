<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Select2</title>
    <style>
        html,
        body {
            height: 100%;
            font-size: 12px;
        }
        pre {
            background-color: lightgray;
            padding: 5px;
        }
        .container {
            min-height: 100%;
            height: auto;
            margin: 0 auto -60px;
            padding: 5px;
            width: 90vw;
        }
    </style>
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.css" rel="stylesheet"/>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/css/select2.css" rel="stylesheet" />
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.js"></script>
    <script src="https://code.jquery.com/jquery-3.4.1.js" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/js/select2.js"></script>
</head>
<body>
<div class="container">
    <h1>Select 2 Demo</h1>
    <?php
    if (isset($_POST['select'])) {
        print '<pre>';
        print 'idx    Code<br/>';
        foreach($_POST['select'] as $key => $attributes) {
            print str_pad($key, 6, ' ', STR_PAD_RIGHT) . ' ' . $attributes['item'] . '<br/>';
        }
        print '</pre>';
    }
    ?>
    <div class="control row" style="border: 1px solid #d3d3d3; margin: 20px 0; padding: 8px;">
        <div class="col-sm-3">
            <span class="align-middle">
                <button id="clone" class="btn btn-sm btn-primary">Clone</button>
            </span>
        </div>
        <div class="col-sm-3">
            <input type="text" id="select-index" class="form-control" readonly title="index" />
        </div>
        <div class="col-sm-3">
            <input type="text" id="select-value" class="form-control" readonly title="value" />
        </div>
        <div class="col-sm-3">
            <input type="text" id="select-text" class="form-control" readonly title="text" />
        </div>
    </div>
    <div class="inventory-form">
        <form id="inventoryForm" method="post">
            <div id="item" class="form-group">
                <label for="select-0-item">Select Item: </label>
                <select id="select-0-item" name="select[0][item]" class="select2-item form-control">
                    <option></option>
                </select>
            </div>
            <div class="form-group">
                <input type="submit" value="Submit" class="btn btn-sm btn-success">
            </div>

        </form>
    </div>
</div>
<script>
    const items = [
        {
            id: 'PEN-1010',
            text: 'Ballpoint Pen'
        },
        {
            id: 'CAL-0010',
            text: 'Calculator'
        },
        {
            id: 'MIS-0210',
            text: 'Eraser'
        },
        {
            id: 'PEN-1020',
            text: 'Fountain Pen'
        },
        {
            id: 'PEN-1110',
            text: 'Highlighter'
        },
        {
            id: 'PEN-2010',
            text: 'Pencil'
        },
        {
            id: 'MIS-1090',
            text: 'Stapler'
        }
    ];
    const select2_options = {
        placeholder: 'select item',
        data:items
    };

    // Set-up the select2 control
    $('#select-0-item').select2(select2_options);

    // Handle the 'Clone' button click event
    $('#clone').click(function(){
        const container = $('#item');
        const master = $('#select-0-item');

        // Get id of new select2 control
        const idx = $('select').length;
        const new_id = 'select-' + idx + '-item';
        const new_name = 'select[' + idx + '][item]';

        // Create a clone of our original select2 but first destroy (or unbind) the data first)
        const clone = master
            .select2('destroy')
            .clone()
            .attr('id', new_id)
            .attr('name', new_name);

        // Append cloned 'select' object to container
        container.append(clone);

        // Initialise 'select' objects
        container.children('select').select2(select2_options);

        // Bind newly cloned select2 to change event
        clone.bind('change', function(event) {
            onSelect2Change(event);
        });
    });

    // On select2 change event
    $('.select2-item').on('change', function(event) {
        onSelect2Change(event)
    });

    function onSelect2Change(e){
        // Update text input controls with index, value and text of select2 that has changed
        $('#select-index').val(e.target.id);
        $('#select-value').val(e.target.value);
        $('#select-text').val($('#' + e.target.id + ' option:selected').text());
    }
</script>
</body>
</html>